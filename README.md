# Gitlab-Slack-Notifications


**Usage:** gitlab-notifications.sh {0 for job succeeded|1 for job failed}

**Slack webhook:**

WEBHOOK=

**Setup instructions:**

Add to your pipeline:

```
before_script:
    - echo "1" > .job_status
  script:
    - *
    - *
    ...
    - echo "0" > .job_status
  after_script:
    - bash <your_location>/gitlab-notifications.sh $(cat .job_status)
```
[Slack message example](https://gitlab.com/bananaseason/gitlab-slack-notifications/blob/master/message_example.jpg)