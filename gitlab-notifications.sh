#!/bin/bash

set -euo pipefail

# Accepts one argument
# FAILURE=1
# SUCCESS=0
# Any other argument - usage statement

case ${1} in
  0)
    STATUS="0"
    ;;
  1)
    STATUS="1"
    ;;
  *)
    echo $"Usage: $0 {0 for job succeeded|1 for job failed}" >&2
    exit 1
esac


############ Test section ##############
# GITLAB_USER_LOGIN=""
# GITLAB_USER_NAME=""
# GITLAB_USER_ID=""
# CI_JOB_NAME=""
# CI_PROJECT_URL=""
# CI_PROJECT_NAME="gitlab-slack-notifications"
# CI_JOB_ID=""
# CI_COMMIT_REF_NAME=master
# STATUS=$1

CHANNEL="123456G"
WEBHOOK="<Paste your webhook here>"
THUMB_URL="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/232/thumbs-up-sign_1f44d.png"
SLACK_MSG_HEADER=":godmode: Deploy to ${CI_JOB_NAME} succeeded"



# Changing message header in case of failure

if [[ "${STATUS}" == "1" ]]; then
   SLACK_MSG_HEADER=":finnadie: Deploy to ${CI_JOB_NAME} failed"
fi

# Message body

SLACK_MSG_BODY="<${CI_PROJECT_URL}|${CI_PROJECT_NAME}> with job <${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}|${CI_JOB_ID}> by ${GITLAB_USER_NAME} \n<${CI_PROJECT_URL}/commit/$(git rev-parse HEAD)|$(git rev-parse --short HEAD)> - ${CI_COMMIT_REF_NAME}"

# Changing picture in case of failure

if [[ "${STATUS}" == "1" ]]
then
  THUMB_URL="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/232/serious-face-with-symbols-covering-mouth_1f92c.png"
fi

# Attachment color depends on the status

if [[ "${STATUS}" == "1" ]]
then
  COLOR="danger"
else
  COLOR="good"
fi

# Message composer

function slack_msg(){
cat<<-EOF
{
    "channel": "${CHANNEL}",
    "attachments": [
        {
            "mrkdwn_in": ["text"],
            "color": "${COLOR}",
            "pretext": "${SLACK_MSG_HEADER}",
            "author_name": "${GITLAB_USER_NAME}",
            "author_link": "https://gitlab.artoxlab.com/${GITLAB_USER_LOGIN}",
            "author_icon": "https://gitlab.artoxlab.com/uploads/-/system/user/avatar/${GITLAB_USER_ID}/avatar.png?width=400",
            "text": "${SLACK_MSG_BODY}",
            "thumb_url": "${THUMB_URL}",
            "footer": "GitLab",
            "footer_icon": "https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png",
            "ts": "$(date "+%s")"
        }
    ]
}
EOF
}

# Message sender

function share_slack_update() {

  local slack_webhook

  slack_webhook=${WEBHOOK}  
  curl -H "Accept: application/json" -X POST --data-urlencode "payload=$(slack_msg)" "${slack_webhook}" &>/dev/null
}

share_slack_update
